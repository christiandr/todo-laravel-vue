require('./bootstrap');

window.Vue = require('vue');

import Form from './Form'
window.Form = Form

Vue.component('todo-form', require('./components/TodoFormComponent.vue').default);

const app = new Vue({
    el: '#app',
});