@extends('layouts.app')


@section('content')

<div class="h-100 w-100 d-flex flex-column justify-content-center align-items-center">
    <todo-form></todo-form>
</div>

@endsection
