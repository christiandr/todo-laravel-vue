<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\Todo;

class TodoTest extends TestCase
{
    /** @test */
    public function aUserCanAddTodo()
    {

        $this->withoutExceptionHandling();

        $response = $this->post('/api/todo', [
            'title' => 'test title',
            'text' => 'test text',
            'status_id' => 1,
        ]);

        $response->assertStatus(200);
    }

    /** @test */
    public function aUserCanEditTodo(){

        $this->withoutExceptionHandling();

        $response = $this->patch('/api/todo/' . Todo::first()->id, [
            'title' => 'updated test title',
            'text' => 'test text',
            'status_id' => 1,
        ]);

        $this->assertEquals('updated test title', Todo::first()->title);

    }

    /** @test */
    public function aUserCanDeleteTodo(){

        $this->withoutExceptionHandling();

        $response = $this->delete('/api/todo/' . Todo::first()->id);

        $this->assertSoftDeleted(Todo::withTrashed()->first());

    }

    //  ./vendor/bin/phpunit --filter aUserCanDeleteTodo

}
