<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Route::get('/test', function () {
    
//     // $todo = App\Models\Todo::first();
//     // dd($todo->status->name);
    
//     // $done = App\Models\Status::find(1);
//     // dd($done->todo);

//     dd(App\Models\Todo::withTrashed()->first());

// });
