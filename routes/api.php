<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TodoController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::resource('todo', TodoController::class);

Route::get('/todo/toggle/{id}', [TodoController::class, 'toggleTodo']);
Route::get('/todo/restore/{id}', [TodoController::class, 'restoreTodo']);
Route::get('/todo/filter/{filter}', [TodoController::class, 'filterTodo']);
