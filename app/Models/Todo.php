<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Todo extends Model
{
    use HasFactory;

    use SoftDeletes;
    
    protected $guarded = [];

    protected $attributes = [
        'status_id' => 2,
    ];

    public function status(){
        return $this->BelongsTo('App\Models\Status');
    }

}
