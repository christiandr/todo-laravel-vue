<?php

namespace App\Http\Controllers;

use App\Models\Todo;
use Illuminate\Http\Request;

class TodoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $todos = Todo::all();
        // $todos = Todo::latest()->get();

        return $todos;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Todo::create($request->all());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Todo  $todo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Todo $todo)
    {
        return $todo->update($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Todo  $todo
     * @return \Illuminate\Http\Response
     */
    public function destroy(Todo $todo)
    {
        return $todo->delete();
    }

    public function toggleTodo($id)
    {

        $todo = Todo::withTrashed()->find($id);

        $status = ($todo->status_id === 1) ?  2 :  1;
        return $todo->update([
            'status_id' => $status,
        ]);

    }

    public function restoreTodo(int $id){
        return Todo::withTrashed()->find($id)->restore();
    }

    public function filterTodo($filter)
    {

        if($filter === "deleted"){
            return Todo::onlyTrashed()->get();
        }

        $filter = ($filter === "done") ?  1 :  2;

        return Todo::where('status_id', $filter)->get();

    }

}
